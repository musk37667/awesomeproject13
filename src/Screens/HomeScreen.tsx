import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button,
} from 'react-native';
import React from 'react';

function HomeScreen({navigation}): JSX.Element {
  return (
    <View style={styles.MainContainer}>
      <View style={styles.FirstContainer}>
        <Text>Hello World</Text>
        <Button
          title="Go to Details"
          onPress={() => navigation.navigate('Details')}
        />
      </View>
    </View>
  );
}

export default HomeScreen;

const styles = StyleSheet.create({
  MainContainer: {
    // backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
  },
  FirstContainer: {
    // backgroundColor: 'pink',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
